@ECHO OFF

PUSHD %~dp0

SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_211

MKDIR build > NUL

SET LIBS=
FOR %%F IN (%CD%\lib\*.jar) DO SET LIBS=!LIBS!;%%F
SET LIBS=%LIBS:~1%

SET CLASSES=
FOR %%F IN (%CD%\build\*.class) DO SET CLASSES=!CLASSES!;%%F
SET CLASSES=%CLASSES:~1%

SET CLASSPATH=%CD%\build;%CD%\lib;%LIBS%;%CLASSES%

javac -d webapp/WEB-INF/classes/ src/*.java

POPD