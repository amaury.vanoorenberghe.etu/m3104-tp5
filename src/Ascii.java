// Servlet Test.java  de test de la configuration
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/Ascii")
public class Ascii extends HttpServlet
{
    private static final int ASCII_MIN = 32;
    private static final int ASCII_MAX = 255;
    private static final int ASCII_COUNT = ASCII_MAX - ASCII_MIN + 1;

    private static final int COL_MAX = 14;

    public void service( HttpServletRequest req, HttpServletResponse res ) 
        throws ServletException, IOException
    {
        int nbCol = 1;

        try {
            String paramNbCol = req.getParameter("nbCol");
            nbCol = Integer.parseInt(paramNbCol);
        } catch (Exception anyExc) {}

        // Sécurité sur les paramètres (red doit être compris entre 0 et 15 (inclu))
        nbCol = Math.min(Math.max(nbCol, 1), COL_MAX);

        res.setContentType("text/html;charset=UTF-8");
        PrintWriter out = res.getWriter();
        out.println("<!doctype html>");
        out.println("<head>");
        out.println("<title>Ascii</title>");
        out.println("<style>td {border: solid 0.1em black;padding: 0.3em;}</style>");
        out.println("<style>kbd {font-weight: bold;}</style>");
        out.println("</head>");
        out.println("<body><center>");

        // FORMULAIRE
        out.println("<form method=\"POST\">");
        out.println("<label for=\"nbCol\">Nombre de colonnes</input>");
        //out.println(String.format("<input name=\"nbCol\" type=\"text\" value=\"%d\"></input>", nbCol));

        out.println("<select name=\"nbCol\">");

        for (int col = 1; col <= COL_MAX; ++col) {
            String selected = col == nbCol ? "selected" : "";

            out.println(String.format("<option value=\"%d\" %s>%d</option>", col, selected, col));
        }

        out.println("</select>");



        out.println("<input type=\"submit\" value=\"OK\"></input>");
        out.println("</form>");


        // TABLEAU
        out.println("<table>");
        String cellData;
        for (int i = 0; i < ASCII_COUNT; ++i) {
            int c = ASCII_MIN + i;

            if (i % nbCol == 0) {
                if (i > 0) {
                    out.println("</tr>");
                }
                out.println("<tr>");
            }

            out.println(String.format("<td>%s</td>", getAscii(c)));
        }

        out.println("</tr></table></center></body>");
    }

    private String getAscii(int code) {
        return String.format("%03d&nbsp;<kbd>%c</kbd>", code, code);
    }
}