// Servlet Test.java  de test de la configuration
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/Palette")
public class Palette extends HttpServlet
{
  public void service( HttpServletRequest req, HttpServletResponse res ) 
       throws ServletException, IOException
  {
    int red = 0;

    try {
        String r = req.getParameter("r");
        red = Integer.parseInt(r);
    } catch (Exception anyExc) {}

    // Sécurité sur les paramètres (red doit être compris entre 0 et 15 (inclu))
    red = Math.min(Math.max(red, 0), 15);

    res.setContentType("text/html;charset=UTF-8");
    PrintWriter out = res.getWriter();
    out.println("<!doctype html>");
    out.println("<head><title>Palette</title></head>");
    out.println("<style>a {margin:0.4em;}</style>");
    out.println("<style>div {margin-bottom:0.8em;}</style>");
    out.println("</head><body><center>");

    out.println("<div>");
    for (int i = 0; i < 16; ++i) {
        out.println(String.format("<a href=\"?r=%d\">%d</a>", i, i));
    }
    out.println("</div>");

    out.println("<table>");
    
    for (int blue = 0; blue < 16; ++blue) {
        out.println("<tr>");

        for (int green = 0; green < 16; ++green) {
            out.println(String.format("<td style=\"width:1em;height:1em;background:#%x%x%x\"></td>", red, green, blue));
        }

        out.println("</tr>");
    }

    out.println("</table></center></body>");
  }
}
