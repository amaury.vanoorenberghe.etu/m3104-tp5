// Servlet Test.java  de test de la configuration
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.util.*;
import java.io.*;
import java.sql.*;

@WebServlet("/ListeRencontres")
public class ListeRencontres extends HttpServlet
{
    private Connection connection = null;

    public void init(ServletConfig config)
        throws ServletException
    {
        super.init();
        try {
            connection = DataSource.instance().getConnection();
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    public void destroy() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void service( HttpServletRequest req, HttpServletResponse res ) 
        throws ServletException, IOException
    {
        res.setContentType("text/html;charset=UTF-8");

        final PrintWriter out = res.getWriter();
        
        try (
            PreparedStatement rencontresSt = connection.prepareStatement("SELECT * FROM rencontres");
            ResultSet rencontres = rencontresSt.executeQuery();

            PreparedStatement equipesSt = connection.prepareStatement("SELECT * FROM equipes");
            ResultSet equipes = equipesSt.executeQuery();
        ) {
            
            Map<Integer, String> teams = new HashMap<>();
            while (equipes.next()) {
                teams.put(equipes.getInt("num_equipe"), equipes.getString("nom_equipe"));
            }
            
            out.println("<!doctype html>");
            out.println("<head>");
            out.println("<title>Liste rencontres sportives</title>");
            out.println("<style>th, td {border: solid 0.1em black;padding: 0.3em;}</style>");
            out.println("</head>");
            out.println("<body><center><table>");

            out.println("<tr>");
            out.println("<th>#</th>");
            out.println("<th>Jour</th>");
            out.println("<th>Equipe A</th>");
            out.println("<th>Score</th>");
            out.println("<th>Equipe B</th>");
            out.println("</tr>");

            while (rencontres.next()) {
                final String date = rencontres.getString("jour");
                final int numMatch = rencontres.getInt("num_match");
                
                final int eq1 = rencontres.getInt("eq1");
                final String eqA = teams.get(eq1);

                final int eq2 = rencontres.getInt("eq2");
                final String eqB = teams.get(eq2);
                
                final int sc1 = rencontres.getInt("sc1");
                final int sc2 = rencontres.getInt("sc2");
                
                System.out.println(String.format("rencontre %s/%s", eqA, eqB));


                out.println("<tr>");
                out.println(String.format("<td>%d</td>", numMatch));
                out.println(String.format("<td>%s</td>", date));
                out.println(String.format("<td>%s (#%04d)</td>", eqA, eq1));
                out.println(String.format("<td>%d &mdash; %d</td>", sc1, sc2));
                out.println(String.format("<td>%s (#%04d)</td>", eqB, eq2));
                out.println("</tr>");
            }

            out.println("</table></center></body>");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
        
        out.println("</center></body>");
    }
}