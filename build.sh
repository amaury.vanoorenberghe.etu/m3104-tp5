#!/bin/bash

export CLASSPATH=$(echo ./lib/*.jar | tr "\n " "::").

OUTPUT=webapp/WEB-INF/
CLASSES=${OUTPUT}classes/
LIB=${OUTPUT}lib/

mkdir -p "${OUTPUT}" > /dev/null

cp -a ./config "${CLASSES}"
cp -a ./lib "${OUTPUT}"

javac -cp "${CLASSPATH}" -d "${CLASSES}" src/*.java